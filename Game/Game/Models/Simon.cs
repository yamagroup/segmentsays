﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Game.Models
{
    public class Simon
    {
        public int ID { get; set; }
        public int UsersCount { get; set; }
        public bool CanStart{ get; set; }
        public bool EndGame{ get; set; }
        public int CurrentQuestion{ get; set; }
        public int TotalAnswers { get; set; }
        public int WrongAnswers { get; set; }
        public virtual ICollection<Question> Questions { get; set; }

        public Simon()
        {
            CurrentQuestion = 1;
            Questions = new Collection<Question>();
        }

    }
}