﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Game.Models;

namespace Game.Controllers
{
    public class SimonController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Simon
        public ActionResult Index()
        {
            return View(db.Simons.ToList());
        }

        // GET: Simon/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Simon simon = db.Simons.Find(id);
            if (simon == null)
            {
                return HttpNotFound();
            }
            return View(simon);
        }

        // GET: Simon/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Simon/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,UsersCount")] Simon simon)
        {
            if (ModelState.IsValid)
            {
                simon.EndGame = false;
                simon.CurrentQuestion = 1;
                db.Simons.Add(simon);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(simon);
        }

        // GET: Simon/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Simon simon = db.Simons.Find(id);
            if (simon == null)
            {
                return HttpNotFound();
            }
            return View(simon);
        }

        // POST: Simon/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,UsersCount")] Simon simon)
        {
            if (ModelState.IsValid)
            {
                simon.CanStart = false;
                simon.CurrentQuestion = 1;
                simon.EndGame = false;
                simon.TotalAnswers = 0;
                simon.WrongAnswers = 0;
                db.Entry(simon).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(simon);
        }

        // GET: Simon/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Simon simon = db.Simons.Find(id);
            if (simon == null)
            {
                return HttpNotFound();
            }
            return View(simon);
        }

        // POST: Simon/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Simon simon = db.Simons.Find(id);
            db.Simons.Remove(simon);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //----------------------------------------------------------

        public ActionResult Play()
        {
            //get cookies




            Simon s = db.Simons.First();
            s.UsersCount++;
            db.SaveChanges();
            return View();


            //Simon s = db.Simons.First();
            //if (s.CanStart == false)
            //{
            //    s.UsersCount++;
            //    db.SaveChanges();
            //    return View();
            //}
            //return null;
        }
        public ActionResult Display()
        {
            return View();
        }

        [HttpPost]
        public ActionResult nextQuestion()
        {
            Simon s = db.Simons.First();
            s.UsersCount = s.UsersCount - s.WrongAnswers;
            db.SaveChanges();
            s.CurrentQuestion++;
            s.TotalAnswers = 0;
            s.WrongAnswers = 0;
            db.SaveChanges();

            return Json("OK");
        }

        [HttpPost]
        public ActionResult getQuestion()
        {
            //get the question and mark the next question as current question
            int currentQuestion = db.Simons.First().CurrentQuestion;

            var questions = from item in db.Questions
                            where item.ID == currentQuestion
                            select item;
            

            if (questions.Count() > 0)
            {
                return Json(questions.First());
            }
            else
            {
                return Json("END");
            }
        }

        [HttpPost]
        public ActionResult getQuestionUser()
        {
            //get the question and mark the next question as current question
            int currentQuestion = db.Simons.First().CurrentQuestion;

            var questions = from item in db.Questions
                            where item.ID == currentQuestion
                            select item;
            if (questions.Count() > 0)
            {
                return Json(questions.First());
            }
            else
            {
                return Json("END");
            }
        }

        [HttpPost]
        public ActionResult getCurrentQuestionId()
        {
            int currentQuestion = db.Simons.First().CurrentQuestion;
            return Json(currentQuestion);
        }

        [HttpPost]
        public ActionResult startSimon()
        {
            try
            {
                Simon s = db.Simons.First();
                s.CanStart = true;
                db.SaveChanges();
                return Json("OK");
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        [HttpPost]
        public ActionResult canStart()
        {
            try
            {
                Simon s = db.Simons.First();
                if (s.CanStart)
                {
                    return Json("true");
                }
                else
                {
                    return Json("false");
                }
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        [HttpPost]
        public ActionResult getUserCount()
        {
            Simon s = db.Simons.First();
            return Json(s.UsersCount);
        }

        [HttpPost]
        public ActionResult getAnswersCount()
        {
            Simon s = db.Simons.First();
            return Json(s.TotalAnswers);
        }

        [HttpPost]
        public ActionResult stopSimon()
        {
            Simon s = db.Simons.First();
            s.EndGame = true;
            db.SaveChanges();
            return Json("Game Ended by moderator.");
        }

        [HttpPost]
        public ActionResult checkGameEnded()
        {
            Simon s = db.Simons.First();
            if (s.EndGame == true)
            {
                return Json("true");
            }
            else
            {
                return Json("false");
            }
        }

        [HttpPost]
        public ActionResult checkAnswer()
        {
            string answer;
            answer = Request.Form["answer"];
            int currentQuestion = db.Simons.First().CurrentQuestion;
            var questions = from item in db.Questions
                            where item.ID == currentQuestion
                            select item;

            Simon s = db.Simons.First();
            s.TotalAnswers++;
            db.SaveChanges();

            if (questions.Count() > 0)
            {
                if (questions.First().CorrectAnswer.Equals(Convert.ToInt16(answer)))
                {
                    

                    return Json("true");
                }
                else
                {
                    Simon s1 = db.Simons.First();
                    s1.WrongAnswers++;
                    s1.UsersCount--;
                    db.SaveChanges();
                    return Json("WRONG ANSWER");
                }
            }
            else
            {
                return Json("END");
            }
        }

    }
}
