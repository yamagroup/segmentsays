﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Game.Models
{
    public class Question
    {
        public int ID { get; set; }

        [Range(1, 100)]
        [Display(Name = "Please set the order for this question")]
        public int Order { get; set; }
        [Required]
        [Display(Name = "Question text")]
        public string Text { get; set; }
        [Required]
        [Display(Name = "Maximum time for answers (in seconds)")]
        [Range(10, Int16.MaxValue)]
        public int Time { get; set; }
        public bool IsDisplaying { get; set; }
        public bool IsNext { get; set; }
        [Required]
        [Display(Name = "Please indicate correct answer (1, 2, 3, 4)")]
        [Range(1, 4)]
        public int CorrectAnswer { get; set; }
    }
}