﻿
var canContinue = false, canStartTimer, gameEnded, questionChanged, counter, canStart = false, segmentClicked = false, changingQuestionAllowed = false;

function startCanStartTimer() {
    canStartTimer = setInterval(function () {
        canStartPlaying();
    }, 20);
}

//checks server to see if can start playing
function canStartPlaying() {
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/canStart',
        //data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            if (response == "true") {
                startSimonUser();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });
}

//sets the screen and starts game
function startSimonUser() {
    clearInterval(canStartTimer);
    $("#currentQuestionId").val(1)
    //hide home
    $(".simon-play-home").addClass("hidden");
    //show game
    $(".simon-game").removeClass("hidden");
    //get first question
    canStart = true;
    displayQuestionUser();
}

function displayQuestionUser() {
    //$(".simon-game-paused").addClass("hidden");
    $(".simon-game").removeClass("hidden");
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/getQuestionUser',
        //data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            //console.log(response);
            displayQuestionUserCallback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });
}

function displayQuestionUserCallback(response) {
    if (response == "END") {
        $("#result").html("Thanks for playing!");
        $(".simon-result").removeClass("hidden");
        $(".simon-game").hide();
        clearInterval(counter);
    } else {
        segmentClicked = false;
        $(".question-text").html(response.Text);
        questionTime = response.Time;
        currentQuestionId = response.Id;
        $(".question-time-left").html(questionTime + " seconds");
        clearInterval(counter);
        counter = setInterval(userTimer, 1000);

        gameEnded = setInterval(function () {
            checkGameEnded();
        }, 1000);

        questionChanged = setInterval(function () {
            checkQuestionChanged();
        }, 200);

    }
}

function userTimer() {
    questionTime = questionTime - 1;
    if (questionTime < 0) {
        if (canContinue == true && segmentClicked == true) {

            displayQuestionUser();
            $(".simon-game-paused").addClass("hidden");
            $(".simon-game").removeClass("hidden");

        } else {
            $(".question-time-left").addClass("hidden");
            $(".simon-game").addClass("hidden");
            clearInterval(counter);
            clearInterval(gameEnded);
            clearInterval(questionChanged);

            for (var i = 1; i < 999; i++) {
                window.clearInterval(i);
            }

            $(".simon-result").removeClass("hidden");
            return;
        }
    }
    $(".question-time-left").html(questionTime + " secs");
}

function checkGameEnded() {
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/checkGameEnded',
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            //console.log(response);
            checkGameEndedCallback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });
}

function checkGameEndedCallback(response) {
    if (response == "true") {
        clearInterval(gameEnded);
        clearInterval(counter);
        $(".simon-game").addClass("hidden");
        $(".simon-game-ended").removeClass("hidden");
        console.log("END GAMEEEEE");
    }
}

function checkQuestionChanged() {
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/getCurrentQuestionId',
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            checkQuestionChangedCallback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });
}

function checkQuestionChangedCallback(response) {
    if (response != $("#currentQuestionId").val()) {
        if (canContinue == true) {
            displayQuestionUser();
            $("#currentQuestionId").val(response);
        } else {

            console.log('end game');
            //end game
            clearInterval(questionChanged);
            clearInterval(counter);
            clearInterval(gameEnded);
            $(".simon-result").removeClass("hidden");
            $(".simon-game").addClass("hidden");
            canContinue = false;
        }
    }
}

function segmentClick(segment) {
    console.log(segment);

    segmentClicked = true;
    var formData = new FormData();
    formData.append('answer', segment);

    //clearInterval(questionChanged);
    //clearInterval(gameEnded);

    //send answer to server and check for the correct answer:
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/checkAnswer',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            segmentClickCallback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });
}

function segmentClickCallback(response) {
    if (response == "true") {
        //correct answer
        $(".simon-game-paused").removeClass("hidden");
        $(".simon-game").addClass("hidden");
        //check for new question
        canContinue = true;
    } else {
        //end game
        clearInterval(questionChanged);

        clearInterval(counter);
        clearInterval(gameEnded);
        $(".simon-result").removeClass("hidden");
        $(".simon-game").addClass("hidden");
        $(".simon-game-paused").addClass("hidden");

        canContinue = false;
    }

}