﻿var currentQuestion, questionTime, counter, canStart = false, userCountTimer, answersTimer;

$(document).ready(function () {
    userCountTimer = setInterval(function () { getUserCount() }, 500);
});

function startSimon() {
    console.log('startSimon');
    clearInterval(userCountTimer);
    answersTimer = setInterval(function () { getAnswersCount(); getUsersLeft(); }, 300);
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/startSimon',
        //data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            console.log(response);
            startSimonCallback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });
}

function startSimonCallback(response) {
    canStart = true;
    getQuestion();
    $(".simon-home").addClass("hidden");
    $(".simon-display").removeClass("hidden");
}

function getUserCount() {
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/getUserCount',
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            getUserCountCallback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });
}

function getUserCountCallback(response) {
    $("#loggedInUsersCount").html(response);
}

function getQuestion() {
    if (canStart == true) {
        $.ajax({
            type: "POST",
            url: baseSiteURL + '/Simon/getQuestion',
            //data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                console.log(response);
                displayQuestionManagerCallback(response);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //Show error message
                console.log(XMLHttpRequest.responseText);
            }
        });
    }
}

function displayQuestionManagerCallback(response) {
    if (response == "END") {
        $(".question-text").html("Thanks for playing!");
        $(".question-time-left").hide();
        clearInterval(counter);
    } else {
        getUsersLeft();
        $(".question-text").html(response.Text);
        questionTime = response.Time;
        $(".question-time-left").html(questionTime + ' <br> <p class="scndsTxt">seconds</p>');
        clearInterval(counter);
        counter = setInterval(timer, 1000);
    }
}

function timer() {
    questionTime = questionTime - 1;
    if (questionTime < 0) {
        clearInterval(counter);
        nextClick();
        return;
    }
    $(".question-time-left").html(questionTime + ' <br> <p class="scndsTxt">seconds</p>');
}

function nextQuestion() {
    getQuestion();
}

function stopSimon() {
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/stopSimon',
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            console.log(response);
            stopSimonCountCallback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });
}

function stopSimonCountCallback(response) {
    $(".simon-display").hide();
    $(".simon-thanks").removeClass("hidden");
    clearInterval(counter);
    clearInterval(answersTimer);
}

function nextClick() {
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/nextQuestion',
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            console.log(response);
            nextClickCallback();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });

}

function nextClickCallback() {
    nextQuestion();
}

function getAnswersCount() {
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/getAnswersCount',
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            getAnswersCountCallback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });
}

function getAnswersCountCallback(response) {
    console.log('getAnswersCount: ' + response);
    $("#usersPlaying").html('' + response);
}


function getUsersLeft() {
    $.ajax({
        type: "POST",
        url: baseSiteURL + '/Simon/getUserCount',
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            getUsersLeftCallback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Show error message
            console.log(XMLHttpRequest.responseText);
        }
    });
}

function getUsersLeftCallback(response) {
    console.log('getUserCount: ' + response);
    if (response == 0) {
        stopSimonCountCallback(response);
    } else {
        $("#totalUsers").html('' + response);
    }
}